
function createCard (name, description, pictureUrl, dateString, locationName) {
    return `
    <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h4 class="card-title">${name}</h4>
            <h5 class="card-subtitle">${locationName}</h5>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <p>${dateString}</p>
        </div>
    </div>
    `;
}

function getDateString (startDate, endDate) {
    let resultString = `${startDate.slice(2, 4)}/${startDate.slice(5, 7)}/${startDate.slice(8, 10)}`;
    resultString += ` - ${endDate.slice(2,4)}/${endDate.slice(5, 7)}/${endDate.slice(8, 10)}`;
    return resultString;
}

function createError (status) {
    return `
    <div class="alert alert-primary" role="alert">
        "Error: status=${status}"
    </div>`
}



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const column = document.querySelector(".col");
            const errorMessage = createError(response.status);
            column.innerHTML = errorMessage;

        } else {
            const data = await response.json();
            let iter = 0
            console.log("Conferences Object: ", data.conference)
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const locationName = details.conference.location.name;
                    const dateString = getDateString(startDate, endDate);
                    const html = createCard(title, description, pictureUrl, dateString, locationName);
                    let index = iter % 3
                    const column = document.querySelectorAll(".col")[index];
                    column.innerHTML += html;
                }
                iter ++;
            }
        }
    } catch (e) {
        console.error(e);
    }

});
