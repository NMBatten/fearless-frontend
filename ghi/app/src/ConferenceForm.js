import React, { useEffect, useState } from 'react';

function ConferenceForm () {

    const [locations, setLocations] = useState([]);

    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        })
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            })

        }

    }

    const fetchData = async () => {
        const locationUrl = "http://localhost:8000/api/locations";
        const response = await fetch(locationUrl);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        } else {
            console.error("Bad response: ", response.status);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>

                    <form onSubmit={handleSubmit} id="create-conference-form">

                        <div className="form-floating mb-3">
                            <input value={formData.name} onChange={handleFormChange} name="name" placeholder="Name" type="text" id="name" className="form-control" required />
                            <label htmlFor="name">Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={formData.starts} onChange={handleFormChange} name="starts"  placeholder="" type="date" id="starts" className="form-control" required />
                            <label htmlFor="starts">Start Date</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input value={formData.ends} onChange={handleFormChange} name="ends" placeholder="" type="date" id="ends" className="form-control" required />
                            <label htmlFor="ends">End Date</label>
                        </div>

                        <div className="mb-3">
                            <textarea value={formData.description} onChange={handleFormChange} id="description" name="description" className="form-control">Describe the Conference!</textarea>
                            <label htmlFor="description">Description</label>
                        </div>

                        <div>
                            <input value={formData.max_presentations} onChange={handleFormChange} type="number" id="max_presentations" name="max_presentations" className="form-control" required />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>

                        <div>
                            <input value={formData.max_attendees} onChange={handleFormChange} type="number" id="max_attendees" name="max_attendees" className="form-control" required />
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleFormChange} name="location" id="location" className="form-select" required>
                                <option defaultValue="" value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name};
                                        </option>
                                    )
                                })};
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>

                </div>
            </div>
        </div>
    )
}

export default ConferenceForm;
